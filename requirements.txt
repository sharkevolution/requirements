jupyterlab==3.5.0
ipyparallel==8.4.1
ipywidgets==8.0.2
ipyleaflet==0.17.2
ipydatagrid==1.1.13

panel==0.14.1   
matplotlib==3.6.2
python-dateutil		# matplotlib, pandas
cycler            	# via matplotlib
kiwisolver         	# via matplotlib
pyparsing          	# via matplotlib
pytz              	# via pandas
scipy              	# via seaborn
six               	# via cycler, python-dateutil
numpy
pandas
seaborn
sphinx-gallery

openpyxl
geocoder
tqdm
networkx
beautifulsoup4
img2pdf
pdf2image
copyDF
websockets

# ipython
# jupyter
# notebook
# ipython